const getSum = (str1, str2) => {
  let array = [str1,str2];
  for (const iterator of array) {
    if(Number(iterator).toString() == "NaN" || typeof iterator != 'string')
      return false;
  }
  return (BigInt(str1)+BigInt(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let result = {
    post: 0,
    comments: 0,
  };
  for (const post of listOfPosts) {
    if(post.author == authorName)
      result.post++;
    
    if(post['comments'] === undefined)
      continue;
    
    for (const comment of post.comments) {
      if(comment.author == authorName)
        result.comments++;
    }
  }
  return 'Post:'+result.post + ',comments:' + result.comments;
};

const tickets=(people)=> {
  let change = 0;
  for (const person of people) {
    let personChange = person - 25;
    if(personChange > change)
      return 'NO';
    change += personChange == 0 ? person : personChange;
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
